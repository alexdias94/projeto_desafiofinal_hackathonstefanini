import express, { NextFunction, Request, Response } from 'express';
import CursoController from '../controllers/curso.controller';
import Aluno from '../entities/aluno.entity';
import Curso from '../entities/curso.entity';
import Mensagem from '../utils/mensagem';

const router = express.Router();

router.post('/curso', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const mensagem: Mensagem = await new CursoController().incluir(req.body);
    res.json(mensagem);
  } catch (e) {
    next(e);
  }
});

router.put('/curso/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const mensagem: Mensagem = await new CursoController().alterar(Number(id), req.body);
    res.json(mensagem);
  } catch (e) {
    next(e);
  }
});

router.delete('/curso/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const mensagem: Mensagem = await new CursoController().excluir(Number(id));
    res.json(mensagem);
  } catch (e) {
    next(e);
  }
});

router.get('/curso/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const curso: Curso = await new CursoController().obterPorId(Number(id));
    res.json(curso);
  } catch (e) {
    next(e);
  }
});

router.get('/curso', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const cursos: Curso[] = await new CursoController().listar();
    res.json(cursos);
  } catch (e) {
    next(e);
  }
});

//Matricular Alunos no curso de id especificado 
router.post('/curso/:idCurso/aluno/:idAluno', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { idCurso,  idAluno} = req.params;
    const mensagem: Mensagem = await new CursoController().vincularCursoAluno(Number(idCurso), Number(idAluno))
    res.json(mensagem);
  } catch (e) {
    next(e);
  }
});

//Vincular Professor no curso de id especificado 
router.post('/curso/:idCurso/professor/:idProfessor', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { idCurso,  idProfessor} = req.params;
    const mensagem: Mensagem = await new CursoController().vincularCursoProfessor(Number(idCurso), Number(idProfessor))
    res.json(mensagem);
  } catch (e) {
    next(e);
  }
});

router.post('/curso/:idCurso/aula', async (req: Request, res: Response, next: NextFunction) => {
  const {idCurso} = req.params
  try {
    const mensagem: Mensagem = await new CursoController().incluirAula(Number(idCurso), req.body)
    res.json(mensagem);
  } catch (e) {
    next(e);
  }
});

export default router;
