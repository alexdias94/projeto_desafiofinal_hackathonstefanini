import Aluno from '../entities/aluno.entity';
import Usuario from '../entities/usuario.entity';
import AlunoRepository from '../repositories/aluno.repository';
import { FilterQuery } from '../utils/database/database';
import { v4 } from 'uuid';
import Mensagem from '../utils/mensagem';
import { TipoUsuario } from '../utils/tipo-usuario.enum';
import { Validador } from '../utils/utils';
import Curso from '../entities/curso.entity';
import cursoRepository from '../repositories/curso.repository';
import alunoRepository from '../repositories/aluno.repository';

export default class AlunoController {
  async obterPorId(id: number): Promise<Aluno> {
    Validador.validarParametros([{ id }]);
    let aluno:Aluno = await AlunoRepository.obterPorId(id);
    Validador.validarUsuario(aluno, TipoUsuario.ALUNO)
    delete aluno.senha
    return aluno
  }

  async obter(filtro: FilterQuery<Aluno> = {}): Promise<Aluno> {
    return await AlunoRepository.obter(filtro);
  }

  // #pegabandeira
  //Colocando o filtro no metodo listar, para retornar objetos que tenham o tipo = Professor / 1!
  async listar(filtro: FilterQuery<Aluno> = {tipo: TipoUsuario.ALUNO}): Promise<Aluno[]> {
    //instanciando um Array de Alunos com os valores retornados pela consulta Listar
    let alunos:Aluno[] = await AlunoRepository.listar(filtro);

    //Mapeando o objeto e deletando o campo senha para não exibir no resultado da requisção
    alunos.map((p) => {
      delete p.senha
    });

    return alunos;

  }

  // #pegabandeira
  async incluir(aluno: Aluno) {
    let { nome, formacao, idade, email, senha, cursos } = aluno;
    Validador.validarParametros([{ nome }, { formacao }, { idade }, { email }, { senha }]);
    
    //Buscar Alunos para fazer a verificação do email
    let alunos:Aluno[] = await AlunoRepository.listar({id: TipoUsuario.ALUNO})

    //Validação do Email com regex e verificar se o email ja encontra-se na base de dados
    Validador.validarEmail(email, alunos)

    //Verificando se o curso foi passado no reqbody se não seta como []
    if(cursos == undefined){
      cursos = [];
    }

    let alunoNovo:Aluno = {
      id: undefined,
      nome: nome,
      senha: senha,
      email: email,
      formacao: formacao,
      idade: idade,
      cursos: cursos,
      tipo: TipoUsuario.ALUNO,
      matricula: v4()
    }

    const id = await AlunoRepository.incluir(alunoNovo);
    return new Mensagem('Aluno incluido com sucesso!', {
      id,
    });
  }

  async alterar(id: number, aluno: Aluno) {
    const {nome, idade, senha, formacao, cursos } = aluno;

    Validador.validarParametros([{ id }]);

    let alunoNovo:Aluno = await AlunoRepository.obterPorId(id);
    alunoNovo.nome = nome;
    alunoNovo.idade = idade;
    alunoNovo.senha = senha;
    alunoNovo.formacao = formacao;
    alunoNovo.cursos = cursos

    await AlunoRepository.alterar({ id }, alunoNovo);
    return new Mensagem('Aluno alterado com sucesso!', {
      id,
    });
  }

  async excluir(id: number) {
    let usuario:Usuario = await (await AlunoRepository.obterPorId(id));
    Validador.validarParametros([{ id }]);

    Validador.validarUsuario(usuario, TipoUsuario.ALUNO);

    await AlunoRepository.excluir({id});
    return new Mensagem('Aluno excluido com sucesso!', { id });
  }

  async vincularAlunoCurso(idAluno:number, idCurso:number, ){

    let curso:Curso = await cursoRepository.obterPorId(idCurso);
    let aluno:Aluno = await alunoRepository.obterPorId(idAluno);

    Validador.validarUsuario(aluno, TipoUsuario.ALUNO)

    //Adicionando o Curso no Aluno
    let cursoNovo:Object = {
      titulo: curso.titulo,
      codCurso: curso.codCurso
    }
    
    if(aluno.cursos == undefined){
      aluno.cursos = []
    }
    aluno.cursos.push(cursoNovo)
    await alunoRepository.alterar({id: idAluno}, aluno)

    //Adicionando o Aluno no Curso
    let alunoNovo:Object = {
      nome: aluno.nome,
      matricula: aluno.email,
    }
   
    if(curso.alunos == undefined){
      curso.alunos = []
    }
    curso.alunos.push(alunoNovo);
    await cursoRepository.alterar({id: idCurso}, curso)


    return new Mensagem(`Aluno: ${aluno.nome} matriculados no Curso ${curso.titulo} com sucesso!`);
  }
}
