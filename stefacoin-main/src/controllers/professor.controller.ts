import Professor from '../entities/professor.entity';
import ProfessorRepository from '../repositories/professor.repository';
import { FilterQuery } from '../utils/database/database';
import Mensagem from '../utils/mensagem';
import { TipoUsuario } from '../utils/tipo-usuario.enum';
import { Validador } from '../utils/utils';
import { v4 } from 'uuid';
import Usuario from '../entities/usuario.entity';
import CursoRepository from '../repositories/curso.repository';
import Curso from '../entities/curso.entity';

export default class ProfessorController {

  async obterPorId(id: number): Promise<Professor> {
    Validador.validarParametros([{ id },]);
    let professor:Professor = await ProfessorRepository.obterPorId(id);
    Validador.validarUsuario(professor, TipoUsuario.PROFESSOR)
    delete professor.senha
    return professor;
  }

  async obter(filtro: FilterQuery<Professor> = {}): Promise<Professor> {
    let professor:Professor = await ProfessorRepository.obter(filtro);
    delete professor.senha
    return professor;
  }

  // #pegabandeira
  //Colocando o filtro no metodo listar, para retornar objetos que tenham o tipo = Professor / 1!
  async listar(filtro: FilterQuery<Professor> = {tipo: TipoUsuario.PROFESSOR}): Promise<Professor[]> {

    //instanciando um Array de Professores com os valores retornados pela consulta Listar
    let professores:Professor[] = await ProfessorRepository.listar(filtro);

    //Mapeando o objeto e deletando o campo senha para não exibir no resultado da requisção
    professores.map((p) => {
     delete p.senha
    })

    return professores;
  }

  // #pegabandeira
  async incluir(professor: Professor) {
    let { cpf, cursos, nome, email, senha} = professor;

    Validador.validarParametros([{ nome }, { email }, { senha }, { cpf }]);

    //Buscando Professores para poder fazer a validação do email
    let professores:Professor[] = await ProfessorRepository.listar({id: TipoUsuario.PROFESSOR})
    
    //Validação do Email com regex e verificar se o email ja encontra-se na base de dados
    Validador.validarEmail(email, professores);

    //Verificando se o curso foi passado no reqbody se não seta como []
    if(cursos == undefined){
      cursos = [];
    }

    //Criando novo objeto para ser persistido e setados os dados passados na requisição
    let professorNovo:Professor = {
      id: undefined,
      codProfessor: v4(),
      nome: nome,
      cpf: cpf,
      email: email,
      senha: senha,
      cursos: cursos,
      tipo: TipoUsuario.PROFESSOR
    }

    const id = await ProfessorRepository.incluir(professorNovo);

    return new Mensagem('Professor incluido com sucesso!', { id });
  }

  async alterar(id: number, professor: Professor) {
    const { nome , senha, cursos } = professor;
    
    Validador.validarParametros([{ id }]);

    //Buscando Obj da Base e setando os novos valores atribuidos na rota
    let professorNovo:Professor = await ProfessorRepository.obterPorId(id);
    professorNovo.nome = nome;
    professorNovo.senha = senha;
    professorNovo.cursos = cursos;
    
    await ProfessorRepository.alterar({ id }, professorNovo);

    return new Mensagem('Professor alterado com sucesso!', { id });
  }

  async excluir(id: number) {

    let usuario:Usuario = await ProfessorRepository.obterPorId(id);
    Validador.validarParametros([{ id }]);

    Validador.validarUsuario(usuario, TipoUsuario.PROFESSOR)

    await ProfessorRepository.excluir({id});

    return new Mensagem('Professor excluido com sucesso!', { id });
  }
  //Vincular Professor ao Curso
  async vincularProfessorCurso(idProfessor:number, idCurso:number, ){

    let professor:Professor = await ProfessorRepository.obterPorId(idProfessor);
    let curso:Curso = await CursoRepository.obterPorId(idCurso);
   
    Validador.validarUsuario(professor, TipoUsuario.PROFESSOR);


    //Adicionando o Curso no Aluno
    let cursoNovo:Object = {
      titulo: curso.titulo,
      codCurso: curso.codCurso
    }
    
    if(professor.cursos == undefined){
      professor.cursos = []
    }
    professor.cursos.push(cursoNovo)
    await ProfessorRepository.alterar({id: idProfessor}, professor)

    //Adicionando o Aluno no Curso
    let professorNovo:Object = {
      nome: professor.nome,
      codProfessor: professor.codProfessor,
    }
   
    if(curso.professores == undefined){
      curso.professores = []
    }
    curso.professores.push(professorNovo);
    await CursoRepository.alterar({id: idCurso}, curso)


    return new Mensagem(`Professor: ${professor.nome} registrado para lecionar o Curso: ${curso.titulo} com sucesso!`);
  }

}
