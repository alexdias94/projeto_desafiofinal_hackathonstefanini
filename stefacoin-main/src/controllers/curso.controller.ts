import Curso from '../entities/curso.entity';
import { v4 } from 'uuid';
import CursoRepository from '../repositories/curso.repository';
import AlunosRepository from '../repositories/aluno.repository';
import { FilterQuery } from '../utils/database/database';
import Mensagem from '../utils/mensagem';
import { Validador } from '../utils/utils';
import Aluno from '../entities/aluno.entity';
import Professor from '../entities/professor.entity';
import professorRepository from '../repositories/professor.repository';
import Aula from '../models/aula.model';
import AulaController from './aula.controller';
import { TipoUsuario } from '../utils/tipo-usuario.enum';

export default class CursoController {
  async obterPorId(id: number): Promise<Curso> {
    Validador.validarParametros([{ id }]);
    return await CursoRepository.obterPorId(id);
  }

  async obter(filtro: FilterQuery<Curso> = {}): Promise<Curso> {
    return await CursoRepository.obter(filtro);
  }

  async listar(filtro: FilterQuery<Curso> = {}): Promise<Curso[]> {
    return await CursoRepository.listar(filtro);
  }

  async incluir(curso: Curso) {
    
    const { titulo, descricao, aulas, professores, alunos } = curso;

    //Validação dos campos obrigatorios
    Validador.validarParametros([{ titulo }, { descricao }]);

    //Setando valores em um novo objeto que será persistido
    let cursoNovo:Curso  = {
      id: undefined,
      codCurso: v4(),
      titulo: titulo,
      descricao: descricao,
      aulas: aulas,
      professores: professores,
      alunos: alunos
    }

    //Recuperando a lista de cursos, para verificar se já existe um curso de mesmo nome na base
    let cursos:Curso[] = await CursoRepository.listar();

    //Validando se o nome do curso e valido
    Validador.validarCurso(titulo, cursos);

    const id = await CursoRepository.incluir(cursoNovo);

    return new Mensagem('Curso incluido com sucesso!', { id });
  }

  async alterar(id: number, curso: Curso) {
    const { titulo, descricao, aulas } = curso;
    Validador.validarParametros([{ id }, { titulo }, { descricao }]);

    //Resgatando o codCurso do Objeto a ser alterado (valor Original)
    let cursos:Curso[] = await CursoRepository.listar();

    //Setando os valores do novo objeto a ser persistido, mantendo o codCurso original
    let cursoNovo:Curso = {
      id: undefined,
      codCurso: cursos[id - 1].codCurso,
      titulo: titulo,
      descricao: descricao,
      aulas: aulas,
    }

    Validador.validarCurso(titulo, cursos)

    await CursoRepository.alterar({ id }, cursoNovo);

    return new Mensagem('Curso alterado com sucesso!', {
      id,
    });
  }

  // *** Fazer regra de exclusão de curso  *** 
  async excluir(id: number) {
    Validador.validarParametros([{ id }]);

    await CursoRepository.excluir({ id });

    return new Mensagem('Curso excluido com sucesso!', {
      id,
    });
  }

  //Vinculando Curso e Aluno
  async vincularCursoAluno(idCurso:number, idAluno:number){

    let curso:Curso = await CursoRepository.obterPorId(idCurso);
    let aluno:Aluno = await AlunosRepository.obterPorId(idAluno);

    Validador.validarUsuario(aluno, TipoUsuario.ALUNO)

    //Adicionando o Aluno no Curso
    let alunoNovo:Object = {
      nome: aluno.nome,
      matricula: aluno.matricula,
    }
   
    if(curso.alunos == undefined){
      curso.alunos = []
    }
    curso.alunos.push(alunoNovo);
    await CursoRepository.alterar({id: idCurso}, curso)

    //Adicionando o Curso no Aluno
    let cursoNovo:Object = {
      titulo: curso.titulo,
      codCurso: curso.codCurso
    }
    
    if(aluno.cursos == undefined){
      aluno.cursos = []
    }
    aluno.cursos.push(cursoNovo)
    await AlunosRepository.alterar({id: idAluno}, aluno)

    return new Mensagem(`Aluno: ${aluno.nome} matriculados no Curso ${curso.titulo} com sucesso!`);

  }

  //Vinculando Curso e Professor
  async vincularCursoProfessor(idCurso:number, idProfessor:number){

    let curso:Curso = await CursoRepository.obterPorId(idCurso);
    let professor:Professor = await professorRepository.obterPorId(idProfessor);

    Validador.validarUsuario(professor, TipoUsuario.PROFESSOR);

    //Adicionando o Aluno no Curso
    let professorNovo:Object = {
      nome: professor.nome,
      codProfessor: professor.codProfessor,
    }
   
    if(curso.professores == undefined){
      curso.professores = []
    }
    curso.professores.push(professorNovo);
    await CursoRepository.alterar({id: idCurso}, curso)

    //Adicionando o Curso no Professor
    let cursoNovo:Object = {
      titulo: curso.titulo,
      codCurso: curso.codCurso
    }
    
    if(professor.cursos == undefined){
      professor.cursos = []
    }
    professor.cursos.push(cursoNovo)
    await professorRepository.alterar({id: idProfessor}, professor)

    return new Mensagem(`Professor: ${professor.nome} esta registrado para lecionar no Curso ${curso.titulo}!`);

  }

  //Incluir Aula no Curso
  async incluirAula(idCurso:number, aula: Aula) {
    const { nome, duracao, topicos} = aula;
    aula.idCurso = idCurso;

    return new AulaController().incluir(aula);
  }
}
