import bcrypt from 'bcryptjs';
import Curso from '../entities/curso.entity';
import Usuario from '../entities/usuario.entity';
import Aluno from '../entities/aluno.entity';
import BusinessException from '../utils/exceptions/business.exception';
import UnauthorizedException from '../utils/exceptions/unauthorized.exception';
import { TipoUsuario } from './tipo-usuario.enum';

export const Validador = {
  validarParametros: (parametros: any[]) => {
    if (!parametros) return true;

    const parametrosInvalidos = parametros
      .filter((p) => {
        const attr = Object.keys(p)[0];
        return (
          p[attr] === null ||
          p[attr] === undefined ||
          (typeof p[attr] === 'number' && isNaN(p[attr])) ||
          (typeof p[attr] === 'string' && p[attr] === '')
        );
      })
      .map((p) => Object.keys(p)[0]);

    if (parametrosInvalidos.length) {
      throw new BusinessException(`Os seguintes parametros são obrigatórios: ${parametrosInvalidos.join(', ')}`);
    }
    return true;
  },


  validarSenha: (senha: string, senhaAtual: string) => {
    const isValid = bcrypt.compareSync(senha, senhaAtual);

    if (!isValid) {
      throw new UnauthorizedException('Usuário ou senha inválida.');
    }
  },

  criptografarSenha: (senha: string): string => {
    return bcrypt.hashSync(senha, 8);
  },

  //Validação do Email com REGEX e verifica se o email ja existe na base
  validarEmail: (email: string, usuarios:Usuario[]) => {
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/

    if(!(reg.test(email))){
      throw new BusinessException("Email " + email +" inválido")
    }

    usuarios.forEach((user) => {
      if(user.email.match(email)){
        throw new BusinessException("Email " + email +" já existe na base de dados")
      }
    });
  },

  validarCursoInserio: (codCurso: string, cursos:Curso[]) => {

    cursos.forEach((curso) => {
      if(curso.codCurso.match(codCurso)){
        throw new BusinessException("Curso " + curso.titulo +" ja esta vinculado a este usuario")
      }
    });
  },

  validarUsuario: (usuario: Usuario, tipo:TipoUsuario) => {
    if(usuario.tipo != tipo ){
      throw new UnauthorizedException("Transação não permitida para este usuario!")
    }
  },


  validarCurso: (titulo:string, cursos:Curso[]) => {
    cursos.forEach((curso) => {
      if(curso.titulo.match(titulo)){
        throw new BusinessException(`Curso com o titulo ${titulo} já existente na base`);
      }
    })
  },


};
