import Exception from './exception';

export default class ConflictException extends Exception {
  constructor(message: string, status: number = 409) {
    super(message, status);
  }
}
