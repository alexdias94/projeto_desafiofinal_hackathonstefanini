import { v4 } from 'uuid'
import Usuario from './usuario.entity';

export default class Professor extends Usuario {
  cpf: string;
  codProfessor: v4;

  constructor() {
    super();
  }
}
