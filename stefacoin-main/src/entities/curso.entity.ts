import { v4 } from 'uuid';
import Aula from '../models/aula.model';
import Aluno from './aluno.entity';
import Entity from './entity';
import Professor from './professor.entity';

export default class Curso extends Entity {
  
  titulo: string;
  descricao: string;
  professores?: Object[];
  aulas?: Aula[];
  alunos?:Object[];
  codCurso: v4;

  constructor() {
    super();
  }
}
