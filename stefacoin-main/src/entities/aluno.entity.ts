import { v4 } from 'uuid';
import Usuario from './usuario.entity';

export default class Aluno extends Usuario {
  idade: string;
  formacao: string;
  matricula: v4;

  constructor() {
    super();
  }
}
