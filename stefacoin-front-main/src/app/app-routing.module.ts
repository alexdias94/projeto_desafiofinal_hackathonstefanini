import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './guards/auth-guard.service';
import { ListarAlunoComponent } from './pages/private/aluno/listar-aluno/listar-aluno.component';
import { ListarCursoComponent } from './pages/private/curso/listar-curso/listar-curso.component';
import { HomeComponent } from './pages/private/home/home.component';
import { CadastrarProfessorComponent } from './pages/private/professor/cadastrar-professor/cadastrar-professor.component';
import { ListarProfessorComponent } from './pages/private/professor/listar-professor/listar-professor.component';
import { CadastroAlunoComponent } from './pages/public/cadastro/aluno/cadastro-aluno.component';
import { CadastroProfessorComponent } from './pages/public/cadastro/professor/cadastro-professor/cadastro-professor.component';
import { LoginComponent } from './pages/public/login/login.component';
import { PaginaNaoEncontradaComponent } from './pages/public/pagina-nao-encontrada/pagina-nao-encontrada.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: HomeComponent,
  },
  {
    path: 'novo-aluno',
    component: CadastroAlunoComponent,
  },
  {
    path: 'novo-professor',
    component: CadastroProfessorComponent,
  },
  {
    path: 'professor/:id',
    component: CadastroProfessorComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'aluno',
    component: ListarAlunoComponent,
  },
  {
    path: 'aluno/:id',
    component: CadastroAlunoComponent,
  },
  {
    path: 'aluno/cadastro',
    component: CadastroAlunoComponent,
  },
  {
    path: 'professor',
    component: ListarProfessorComponent,
  },
  {
    path: 'professor/cadastro',
    component: CadastrarProfessorComponent,
  },
  {
    path: 'curso',
    component: ListarCursoComponent,
  },
  {
    path: '**',
    component: PaginaNaoEncontradaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
