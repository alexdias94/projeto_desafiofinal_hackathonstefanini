import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-professor',
  templateUrl: './cadastro-professor.component.html',
  styleUrls: ['./cadastro-professor.component.css']
})
export class CadastroProfessorComponent implements OnInit {

  abas = [
    {index: 0, title: 'Aluno', class: 'nav-link', href: 'novo-aluno'},
    {index: 1, title: 'Professor', class: 'nav-link active', href: 'novo-professor'},
  ]

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

}

