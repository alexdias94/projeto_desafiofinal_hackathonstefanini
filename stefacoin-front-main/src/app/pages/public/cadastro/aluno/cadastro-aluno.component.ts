import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-aluno',
  templateUrl: './cadastro-aluno.component.html',
  styleUrls: ['./cadastro-aluno.component.css']
})
export class CadastroAlunoComponent implements OnInit {

  abas = [
    {index: 0, title: 'Aluno', class: 'nav-link active', href: 'novo-aluno'},
    {index: 1, title: 'Professor', class: 'nav-link', href: 'novo-professor'},
  ]


  seletorFrom = [

  ]

  constructor(
    private router: Router
  ) { }

  mudarActivade(){

  }

  ngOnInit(): void {
  }

}
