import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Curso } from 'src/app/models/curso';
import { CursoService } from 'src/app/services/curso.service';

@Component({
  selector: 'app-listar-curso',
  templateUrl: './listar-curso.component.html',
  styleUrls: ['./listar-curso.component.css']
})
export class ListarCursoComponent implements OnInit {
  cursos:Curso[]

  constructor(
    private service:CursoService,
    private toastr:ToastrService
  ) { }

  ngOnInit(): void {
    this.service.listar().subscribe((lista) => {
      this.cursos = lista;
      console.log(this.cursos);
    })
  }

  excluir(curso:Curso){
    this.service.excluir(String(curso.id)).subscribe((response) => {
      this.toastr.success(response.mensagem);
      this.cursos = [];
      this.service.listar().subscribe((lista) => {
        this.cursos = lista
      })
    },
      (err) => this.toastr.error(err.mensagem)
    );
  }

}
