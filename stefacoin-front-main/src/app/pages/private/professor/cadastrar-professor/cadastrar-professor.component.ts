import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Mensagem } from 'src/app/models/mensagem';
import { Professor } from 'src/app/models/professor';
import { ProfessorService } from 'src/app/services/professor.service';
import { Utils } from 'src/app/utils';
import { PasswordValidator } from 'src/app/validators/password-validator';

@Component({
  selector: 'app-cadastrar-professor',
  templateUrl: './cadastrar-professor.component.html',
  styleUrls: ['./cadastrar-professor.component.css']
})
export class CadastrarProfessorComponent implements OnInit {

  professor: Professor;

  cadastroProfessorFrom: FormGroup;
  
  matching_passwords_group: FormGroup;
  validation_messages = {
    'nome': [
      { type: 'required', message: 'Nome é obrigatório.' },
      { type: 'minlength', message: 'Nome mínimo 3 ' },
    ],
    'email': [
      { type: 'required', message: 'Email é obrigatório.' },
      { type: 'email', message: 'Entre com um email válido.' }
    ],
    'cpf': [
      { type: 'required', message: 'CPF é obrigatório.' },
      { type: 'minlength', message: 'CPF mínimo 11' },
      { type: 'pattern', message: 'Adicionar CPF no formato 000.000.000-00' }
    ],
    'password': [
      { type: 'required', message: 'Senha é obrigatório.' },
      { type: 'minlength', message: 'Senha teve conter no minimo 8 caracteres.' },
      { type: 'pattern', message: 'Sua senha deve conter no mínimo uma letra maiúscula, uma minúscula e 1 numero.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirmação de senha é obrigatório.' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Senhas incompatíveis.' }
    ],
  };

  constructor(
    private fombuild: FormBuilder,
    private service: ProfessorService,
    private toastr: ToastrService,
    private router: Router,
    private activatedRote: ActivatedRoute
  ) { }

  async ngOnInit() {
    const id = this.activatedRote.snapshot.paramMap.get('id')
    await this.obterUsuario(id)
    this.buildForm();

  }

  async obterUsuario(id?: any){
    if(id){
     this.professor = await this.service.obterPorId(parseInt(id)).toPromise();
    }
  }

  buildForm() {
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(8),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });

    this.cadastroProfessorFrom = this.fombuild.group({
      nome: [!!this.professor && !!this.professor.nome? this.professor.nome : null, Validators.compose([
        Validators.minLength(3),
        Validators.required
      ]),],
      email: [!!this.professor && !!this.professor.email? this.professor.email : null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      cpf: [ !!this.professor && !!this.professor.cpf? this.professor.cpf : null, Validators.compose([
        Validators.required,
        Validators.minLength(11),
      ])],
      matching_passwords: this.matching_passwords_group,
    })
  }

  aplicarMascaraCPF(){
   
    this.cadastroProfessorFrom.controls.cpf.setValue(Utils.mascaraCPF(this.cadastroProfessorFrom.controls.cpf.value))
  }

  showMsgSucces(msg:string){
    this.toastr.success(msg)
  }
  showMsgError(msg:string){
    this.toastr.error(msg)
  }

  cadastrar() {
    this.professor = this.cadastroProfessorFrom.value;
    this.professor.senha = this.cadastroProfessorFrom.get('matching_passwords').get('password').value;

    if(this.cadastroProfessorFrom.valid){
      this.service.cadastrar(this.professor).subscribe(
        (response) => {
          this.showMsgSucces(response.mensagem)
          this.navegar();
        },
        (err) => {
          this.showMsgError(err.error.message)
        },
      );
    } else {
      Utils.verificarValidacaoForm(this.cadastroProfessorFrom)
    }
  }

  navegar(): void {
    this.router.navigate(['professor'])
  }
  alterarProfessor(){
    this.service.alterar(this.professor).pipe(take(1))
    .subscribe((resp:Mensagem) => {
      this.showMsgSucces(resp.mensagem)
    },err => this.showMsgError(err.error.message));
  }

  submitForm(){
    if(this.cadastroProfessorFrom.valid){
      this.professor = this.cadastroProfessorFrom.value;
      this.professor.senha = this.cadastroProfessorFrom.value.senha.password;
      if(this.professor && this.professor.id){
        this.alterarProfessor();
      }else{
        this.cadastrar()
       
      }
    }else{
      Utils.verificarValidacaoForm(this.cadastroProfessorFrom);
    }

   
  }

}