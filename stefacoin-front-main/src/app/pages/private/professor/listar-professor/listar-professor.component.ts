import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Professor } from 'src/app/models/professor';
import { ProfessorService } from 'src/app/services/professor.service';

@Component({
  selector: 'app-listar-professor',
  templateUrl: './listar-professor.component.html',
  styleUrls: ['./listar-professor.component.css']
})
export class ListarProfessorComponent implements OnInit {
  professores: Professor[];
  
  constructor(
    private service:ProfessorService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.service.listar().subscribe((lista) => {
      this.professores = lista;
      console.log(this.professores);
    })
  }

  editar(id: string): void  {
    this.router.navigate(['professor/' + id])
  }

  excluir(professor:Professor){
    this.service.excluir(String(professor.id)).subscribe((response) => {
      this.toastr.success(response.mensagem);
      this.professores = [];
      this.service.listar().subscribe((lista) => {
        this.professores = lista
        console.log(this.professores)
      })
    },
      (err) => this.toastr.error(err.mensagem)
    );
  }

}