import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Aluno } from 'src/app/models/aluno';
import { AlunosService } from 'src/app/services/alunos.service';

@Component({
  selector: 'app-listar-aluno',
  templateUrl: './listar-aluno.component.html',
  styleUrls: ['./listar-aluno.component.css']
})

export class ListarAlunoComponent implements OnInit {
  alunos: Aluno[];
  
  constructor(
    private service:AlunosService,
    private router:Router,
    private toastr:ToastrService
  ) {}

  ngOnInit(): void {
    this.service.listar().subscribe((lista) => {
      this.alunos = lista;
      console.log(this.alunos);
    })
  }

  excluir(aluno:Aluno){
    this.service.excluir(String(aluno.id)).subscribe((response) => {
      this.toastr.success(response.mensagem);
      this.alunos = [];
      this.service.listar().subscribe((lista) => {
        this.alunos = lista
        console.log(this.alunos)
      })
    },
      (err) => this.toastr.error(err.mensagem)
    );
  }

  editar(id:string): void  {
    this.router.navigate(['aluno/' + id])
  }

}


