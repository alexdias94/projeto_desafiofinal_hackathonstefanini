import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Aluno } from 'src/app/models/aluno';
import { Mensagem } from 'src/app/models/mensagem';
import { AlunosService } from 'src/app/services/alunos.service';
import { Utils } from 'src/app/utils';
import { PasswordValidator } from 'src/app/validators/password-validator';

@Component({
  selector: 'app-cadastrar-aluno',
  templateUrl: './cadastrar-aluno.component.html',
  styleUrls: ['./cadastrar-aluno.component.css']
})
export class CadastrarAlunoComponent implements OnInit {

  abas = [
    {title: 'Aluno', class: 'nav-link active'},
    {title: 'Professor', class: 'nav-link '},
  ]

  aluno: Aluno;

  cadastroFrom: FormGroup;
  matching_passwords_group: FormGroup;
  validation_messages = {
    'nome': [
        { type: 'required', message: 'Nome é obrigatório.' },
        { type: 'minlength', message: 'Nome minimo3 ' },
    ],
    'email': [
        { type: 'required', message: 'Email é obrigatório.' },
        { type: 'email', message: 'Entre com um email válido.' }
    ],
    'password': [
        { type: 'required', message: 'Senha é obrigatório.' },
        { type: 'minlength', message: 'Senha teve conter no minimo 8 caracteres.' },
        { type: 'pattern', message: 'Sua senha deve conter no mínimo uma letra maiúscula, uma minúscula e 1 numero.' }
    ],
    'confirm_password': [
        { type: 'required', message: 'Confirmação de senha é obrigatório.' }
    ],
    'matching_passwords': [
        { type: 'areEqual', message: 'Senhas incompatíveis.' }
    ],
};

  constructor(
    private formbuild: FormBuilder,
    private service: AlunosService,
    private router: Router,
    private toastr: ToastrService,
    private activatedRote: ActivatedRoute
  ) { }

   async ngOnInit() {
    const id = this.activatedRote.snapshot.paramMap.get('id')
    await this.obterUsuario(id)
    this.buildForm();
    console.log(this.aluno)
  }

   async obterUsuario(id?: any){
     if(id){
      this.aluno = await this.service.obterPorId(parseInt(id)).toPromise();
     }
    
  }

  

 

  navegar(): void {
    this.router.navigate(['aluno'])
  }


  submitForm(){
    if(this.cadastroFrom.valid){
      this.aluno = this.cadastroFrom.value;
      this.aluno.senha = this.cadastroFrom.value.senha.password;
      if(this.aluno && this.aluno.id){
        this.alterarAluno();
      }else{
        this.cadastrar()
       
      }
    }else{
      Utils.verificarValidacaoForm(this.cadastroFrom);
    }

   
  }

  cadastrar() {
    if (this.cadastroFrom.valid) {
      this.service.cadastrar(this.aluno).pipe(take(1))
      .subscribe((resp) => {
        this.showMenssagemSucces(resp.mensagem);
        this.navegar();
      },
        (err) =>  this.showMenssagemError(err.error.message));
    } else {
      Utils.verificarValidacaoForm(this.cadastroFrom)
    }
}

  alterarAluno(){
    this.service.alterar(this.aluno).pipe(take(1))
    .subscribe((resp:Mensagem) => {
      this.showMenssagemSucces(resp.mensagem)
    },err => this.showMenssagemError(err.error.message));
  }
  
  showMenssagemSucces(msg:string){
    this.toastr.success(msg)
  }

  showMenssagemError(msg:string){
    this.toastr.error(msg)
  }

  buildForm(): void {
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
          Validators.minLength(8),
          Validators.required,
          Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
  }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
  });
  this.cadastroFrom = this.formbuild.group({
    nome: [!!this.aluno && this.aluno.nome ? this.aluno.nome : null,
      Validators
      .compose([
      Validators.minLength(3),
      Validators.required
    ]),],
    email: [!!this.aluno && this.aluno.email ? this.aluno.email : null, 
      Validators
      .compose([
      Validators.required,
      Validators.email
    ])],
    idade: [!!this.aluno && this.aluno.idade ? this.aluno.idade: null ],
    formacao: [!!this.aluno && this.aluno.formacao ? this.aluno.formacao:null],
    senha: this.matching_passwords_group,
    });
  }
}


