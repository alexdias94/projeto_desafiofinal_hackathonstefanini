import { FormArray, FormGroup } from "@angular/forms";

export class Utils {
    static verificarValidacaoForm(formGroup: FormGroup): void {
        Object.keys(formGroup.controls).forEach((campo) => {
          const controle = formGroup.get(campo);
          controle.markAsDirty({ onlySelf: true });
          controle.markAsTouched({ onlySelf: true });
          controle.updateValueAndValidity({ onlySelf: true });
          if (controle instanceof FormGroup) {
            this.verificarValidacaoForm(controle);
          }
          if (controle instanceof FormArray) {
            for (const x of controle.controls) {
              if (x instanceof FormGroup) {
                this.verificarValidacaoForm(x);
              }
            }
          }
        });
      }
      static mascaraCPF(v) {
        if (v) {
          v = v.replace(/\D/g, '');
          v = v.replace(/(\d{3})(\d)/, '$1.$2');
          v = v.replace(/(\d{3})(\d)/, '$1.$2');
          v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        }
        return v;
      }
}