export interface Curso {
    
    id?: number,
    titulo: string;
    descricao: string,
    codCurso?: string;
    aulas?: Object[];
}
