export interface Aluno {
    
    id?: number,
    email: string,
    senha: string,
    nome: string,
    cursos?: Object[],
    idade: string,
    formacao: string,
    matricula?: string
}
