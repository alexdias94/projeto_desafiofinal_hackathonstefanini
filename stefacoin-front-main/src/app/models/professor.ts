export interface Professor {

    id?: number;
    nome: string;
    email: string;
    cpf: string;
    codProfessor?: string;
    senha?: string;
}
