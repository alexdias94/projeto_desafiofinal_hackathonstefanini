import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Mensagem } from '../models/mensagem';
import { Professor } from '../models/professor';

const URL = 'http://localhost:3000/stefanini/professor';

@Injectable({
  providedIn: 'root',
})
export class ProfessorService {
  constructor(private httpClient: HttpClient) {}

  // #pegabandeira

  listar(): Observable<Professor[]> {
    return this.httpClient.get<Professor[]>(URL);
  }
   
  
  obter() {}

  incluir(professor: Professor): Observable<Mensagem> {
    return this.httpClient.post<Mensagem>(URL, professor);
  }

  excluir(id: string): Observable<any>{
    return this.httpClient.delete<Mensagem>(
      URL + "/" + id );
  } 
  alterar(professor:Professor): Observable<Mensagem>{
    return this.httpClient.put<Mensagem>(
      URL + "/" + professor.id,
      professor
    );
  }

  cadastrar(professor:Professor): Observable<Mensagem>{
    return this.httpClient.post<Mensagem>(
      URL,
      professor
    );
  }

  obterPorId(id:number): Observable<Professor>{
    return this.httpClient.get<Professor>(URL + "/" + id);
  }
}
