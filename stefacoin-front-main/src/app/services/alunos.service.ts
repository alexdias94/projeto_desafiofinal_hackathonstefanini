import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Aluno } from '../models/aluno';
import { Mensagem } from '../models/mensagem';

const URL = 'http://localhost:3000/stefanini/aluno';


@Injectable({
  providedIn: 'root'
})


export class AlunosService {

  constructor(
    private httpClient:HttpClient
    ) { }

    listar(): Observable<Aluno[]> {
      return this.httpClient.get<Aluno[]>(URL);
    }

    cadastrar(aluno:Aluno): Observable<Mensagem>{
      return this.httpClient.post<Mensagem>(
        URL,
        aluno
      );
    }

    excluir(id: string): Observable<any>{
      return this.httpClient.delete<Mensagem>(
        URL + "/" + id );
    }

    obterPorId(id:number): Observable<Aluno>{
      return this.httpClient.get<Aluno>(URL + "/" + id);
    }

    alterar(aluno:Aluno): Observable<Mensagem>{
      return this.httpClient.put<Mensagem>(
        URL + "/" + aluno.id,
        aluno
      );
    }
}
