import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Curso } from '../models/curso';
import { Mensagem } from '../models/mensagem';

const URL = 'http://localhost:3000/stefanini/curso';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  constructor(
    private httpClient:HttpClient
  ) { }

  listar(): Observable<Curso[]> {
    return this.httpClient.get<Curso[]>(URL);
  }

  cadastrar(curso:Curso): Observable<Mensagem>{
    return this.httpClient.post<Mensagem>(
      URL,
      curso
    );
  }

  excluir(id: string): Observable<any>{
    return this.httpClient.delete<Mensagem>(
      URL + "/" + id );
  } 
}
